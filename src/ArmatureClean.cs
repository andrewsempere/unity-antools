﻿/*
    - Adds menu item Tools/Armature/Clean

    - When invoked, examines the avatar armature and if it finds any children with the same name as the parent, it deletes them. 
      For instance if "Belly" contains child "Belly" it removes the second.

*/
#if (UNITY_EDITOR)
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace An
{
    public class ArmatureClean : MonoBehaviour
    {
        public static void Clean(GameObject obj)
        {
            string path = An.Util.GetGameObjectPath(obj);
            if (
                EditorUtility.DisplayDialog(
                    "Clean Armature?",
                    "This will delete any previously merged armature from"
                        + path
                        + ". Operation cannot be reversed!",
                    "Clean",
                    "Cancel"
                )
            ) {
                Traverse(obj.transform, 0, path);
            }
        }

        public static void Traverse(Transform node, int depth, string path)
        {
            foreach (Transform child in node)
            {
                string currentPath = path + "/" + child.name;
                GameObject targetExists = NodeFindChild(
                    child.name,
                    GameObject.Find(currentPath).transform
                );
                if (targetExists != null)
                {
                    DestroyImmediate(targetExists);
                }
                if (child.childCount > 0)
                {
                    Traverse(child, depth++, currentPath);
                }
            }
        }

        public static GameObject NodeFindChild(string lookingFor, Transform t)
        {
            GameObject found = null;
            foreach (Transform child in t)
            {
                if (child.name == lookingFor)
                {
                    found = child.gameObject;
                    break;
                }
            }
            return found;
        }
    }
}
#endif
