﻿/*
    - Adds menu item Tools/Armature/Copy Blend Shapes

    - Copies blend shapes

*/
#if (UNITY_EDITOR)
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;

namespace An
{
    public static class BlendShapeCopy
    {
        public static void Copy(GameObject srcObj, GameObject destObj, bool tryChildren)
        {
            string srcPath = An.Util.GetGameObjectPath(srcObj);
            string destPath = An.Util.GetGameObjectPath(destObj);

            if (!srcObj.TryGetComponent<SkinnedMeshRenderer>(out var skinnedMeshRendererSrc))
            {
                Debug.LogWarning(srcObj.transform.name + " doesn't contain blend shapes!");
                return;
            }

            if (!destObj.TryGetComponent<SkinnedMeshRenderer>(out var skinnedMeshRendererDst))
            {
                if (tryChildren)
                {
                    foreach (Transform child in destObj.transform)
                    {
                        Copy(srcObj, child.gameObject, false);
                    }
                }
                else
                {
                    Debug.LogWarning(destPath + " doesn't contain blend shapes!");
                }
            }
            else
            {
                string[] destBlendShapes = An.Util.getBlendShapeNames(destObj);
                foreach (string blendShapeName in An.Util.getBlendShapeNames(srcObj))
                {
                    if (Array.Exists(destBlendShapes, element => element == blendShapeName))
                    {
                        int index = An.Util.getBlendShapeIndex(destObj, blendShapeName);
                        if (index > -1)
                        {
                            float weightVal = An.Util.getBlendShapeWeightByName(
                                srcObj,
                                blendShapeName
                            );
                            //Debug.Log("Setting: " + blendShapeName + "=" + weightVal);
                            skinnedMeshRendererDst.SetBlendShapeWeight(index, weightVal);
                        }
                    }
                }
            }
        }

        public static void Zero(GameObject obj, bool tryChildren)
        {
            string objPath = An.Util.GetGameObjectPath(obj);

            if (!obj.TryGetComponent<SkinnedMeshRenderer>(out var skinnedMeshRendererDst))
            {
                if (tryChildren)
                {
                    foreach (Transform child in obj.transform)
                    {
                        Zero(child.gameObject, false);
                    }
                }
                else
                {
                    Debug.LogWarning(objPath + " doesn't contain blend shapes!");
                }
            }
            else
            {
                string[] destBlendShapes = An.Util.getBlendShapeNames(obj);
                foreach (string blendShapeName in An.Util.getBlendShapeNames(obj))
                {
                    if (Array.Exists(destBlendShapes, element => element == blendShapeName))
                    {
                        int index = An.Util.getBlendShapeIndex(obj, blendShapeName);
                        if (index > -1)
                        {
                            skinnedMeshRendererDst.SetBlendShapeWeight(index, 0);
                        }
                    }
                }
            }
        }
    }
}
#endif
