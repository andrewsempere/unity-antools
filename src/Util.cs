#if (UNITY_EDITOR)
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;

namespace An
{
    public static class Util
    {
        public static string GetGameObjectPath(GameObject obj)
        {
            string path = "/" + obj.name;
            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path = "/" + obj.name + path;
            }
            return path;
        }

        public static int getBlendShapeIndex(GameObject obj, string targetName)
        {
            SkinnedMeshRenderer gameObj = obj.GetComponent<SkinnedMeshRenderer>();
            Mesh m = gameObj.sharedMesh;
            string[] arr;
            arr = new string[m.blendShapeCount];
            int index = -1;
            for (int i = 0; i < m.blendShapeCount; i++)
            {
                string blendShapeName = m.GetBlendShapeName(i);
                if (blendShapeName == targetName)
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public static string[] getBlendShapeNames(GameObject obj)
        {
            //https://stackoverflow.com/questions/22694672/get-blendshapes-by-name-rather-than-by-index
            SkinnedMeshRenderer gameObj = obj.GetComponent<SkinnedMeshRenderer>();
            Mesh m = gameObj.sharedMesh;
            string[] arr;
            arr = new string[m.blendShapeCount];
            for (int i = 0; i < m.blendShapeCount; i++)
            {
                string s = m.GetBlendShapeName(i);
                arr[i] = s;
            }
            return arr;
        }

        public static float getBlendShapeWeightByName(GameObject obj, string targetName)
        {
            SkinnedMeshRenderer gameObj = obj.GetComponent<SkinnedMeshRenderer>();
            Mesh m = gameObj.sharedMesh;
            string[] arr;
            arr = new string[m.blendShapeCount];
            for (int i = 0; i < m.blendShapeCount; i++)
            {
                string blendShapeName = m.GetBlendShapeName(i);
                if (blendShapeName == targetName)
                {
                    return gameObj.GetBlendShapeWeight(i);
                }
            }

            return -1;
        }

        public static void WriteToFile(string data, string path)
        {
            bool append = false;
            StreamWriter writer = new StreamWriter(path, append);
            writer.WriteLine(data);
            writer.Close();
            StreamReader reader = new StreamReader(path);
        }

        public static string ReadFromFile(string path)
        {
            StreamReader reader = new StreamReader(path);
            string data = reader.ReadToEnd();
            reader.Close();
            return data;
        }
    }
}
#endif
