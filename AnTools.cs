#if (UNITY_EDITOR)
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using An;

public class AnTools : EditorWindow
{
    private static AnTools _winInstance;

    private static GameObject srcObj;
    private static GameObject destObj;

    private static GameObject armatureSrc;
    private static GameObject armatureDest;

    Boolean[] SectionOpen = new Boolean[2];
    bool applyToChildren = false;

    [MenuItem("Tools/AnTools")]
    public static void ShowWindow()
    {
        _winInstance = GetWindow<AnTools>();
    }

    private void OnGUI()
    {
        // Blend Shape Values
        SectionOpen[0] = EditorGUILayout.BeginFoldoutHeaderGroup(
            SectionOpen[0],
            "BlendShape Values"
        );
        if (SectionOpen[0])
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Source");
            srcObj = EditorGUILayout.ObjectField(srcObj, typeof(GameObject), true) as GameObject;
            GUI.enabled = (srcObj != null);
            if (GUILayout.Button("Export"))
            {
                An.BlendShapeTransfer.ExportJson(srcObj, applyToChildren);
            }
            if (GUILayout.Button("Import"))
            {
                An.BlendShapeTransfer.ImportJson(srcObj);
            }
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Destination");
            destObj = EditorGUILayout.ObjectField(destObj, typeof(GameObject), true) as GameObject;
            GUI.enabled = (destObj != null);
            if (GUILayout.Button("Zero"))
            {
                An.BlendShapeCopy.Zero(destObj, applyToChildren);
            }
            if (GUILayout.Button("Export"))
            {
                An.BlendShapeTransfer.ExportJson(destObj, applyToChildren);
            }
            if (GUILayout.Button("Import"))
            {
                An.BlendShapeTransfer.ImportJson(destObj);
            }

            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            applyToChildren = GUILayout.Toggle(applyToChildren, "Apply to children");
            GUI.enabled = true;
            GUI.enabled = (srcObj != null && destObj != null && !(srcObj == destObj));
            if (GUILayout.Button("Apply"))
            {
                An.BlendShapeCopy.Copy(srcObj, destObj, applyToChildren);
            }
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        // Armature Merge
        SectionOpen[1] = EditorGUILayout.BeginFoldoutHeaderGroup(SectionOpen[1], "Armature Merge");
        if (SectionOpen[1])
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Source");
            armatureSrc =
                EditorGUILayout.ObjectField(armatureSrc, typeof(GameObject), true) as GameObject;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Destination");
            armatureDest =
                EditorGUILayout.ObjectField(armatureDest, typeof(GameObject), true) as GameObject;
            GUI.enabled = (armatureDest != null);
            if (GUILayout.Button("Clean"))
            {
                An.ArmatureClean.Clean(armatureDest);
            }
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUI.enabled = (armatureSrc != null);

            GUI.enabled = false;
            if (armatureSrc != null && armatureDest != null && !(armatureSrc == armatureDest))
            {
                if (
                    (PrefabUtility.GetPrefabAssetType(armatureSrc) == PrefabAssetType.NotAPrefab)
                    && (
                        PrefabUtility.GetPrefabAssetType(armatureDest) == PrefabAssetType.NotAPrefab
                    )
                ) {
                    GUI.enabled = true;
                }
            }
            if (GUILayout.Button("Merge"))
            {
                An.ArmatureMerge.Merge(armatureSrc, armatureDest);
            }
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFoldoutHeaderGroup();
    }
}


#endif
